﻿using System;
using System.Collections;
using UnityEngine;

public class TimerAnimator : MonoBehaviour
{
    [SerializeField] private CanvasGroup timerGroup;
    [SerializeField] private float alphaAnimationSpeed;

    public event Action OnOpenFinish;
    public event Action OnCloseFinish;

    private void Awake()
    {
        timerGroup.alpha = 0;
        timerGroup.gameObject.SetActive(false);
    }

    public void OpenAnimation()
    {
        StartCoroutine(ShowAnimation());
    }

    public void CloseAnimation()
    {
        StartCoroutine(HideAnimation());
    }

    private IEnumerator HideAnimation()
    {
        timerGroup.alpha = 1f;
        for (var a = 1f; a > 0f; a-=Time.deltaTime * alphaAnimationSpeed)
        {
            timerGroup.alpha = a;
            yield return new WaitForSeconds(Time.deltaTime);
        }

        timerGroup.alpha = 0f;
        timerGroup.gameObject.SetActive(false);
        OnCloseFinish?.Invoke();
    }

    private IEnumerator ShowAnimation()
    {
        timerGroup.gameObject.SetActive(true);
        timerGroup.alpha = 0f;
        for (var a = 0f; a < 1f; a+=Time.deltaTime * alphaAnimationSpeed)
        {
            timerGroup.alpha = a;
            yield return new WaitForSeconds(Time.deltaTime);
        }

        timerGroup.alpha = 1f;
        OnOpenFinish?.Invoke();
    }
}
