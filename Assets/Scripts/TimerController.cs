﻿using System;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TimerController : MonoBehaviour
{
    [SerializeField] private TimersController timersController;
    [SerializeField] private float startTimeChangeSpeed;
    [SerializeField] private float deltaTimeChangeSpeed;
    [SerializeField] private TMP_Text timerText;
    [SerializeField] private Button addTimeButton;
    [SerializeField] private Button reduceTimeButton;
    [SerializeField] private Button startTimeButton;
    
    private TimerOpenButton currentTimerButton;
    private bool isAddTime = false;
    private bool isReduceTime = false;
    private float currentTimeChangeSpeed;
    private double timerTime = 0f;
    
    private void Awake()
    {
        timersController.OnCurrentTimerChange += OnChangeCurrentTimer;
    }

    private void Start()
    {
        currentTimeChangeSpeed = startTimeChangeSpeed;
    }

    private void OnChangeCurrentTimer(TimerOpenButton timerOpenButton)
    {
        if (timerOpenButton == null) return;
        currentTimerButton = timerOpenButton;
        ChangeTimerText();
        var buttonInteractable = !timerOpenButton.Timer.IsStart;
        startTimeButton.interactable = buttonInteractable;
        addTimeButton.interactable = buttonInteractable;
        reduceTimeButton.interactable = buttonInteractable;
        var currentTime = DateTime.Now;
        var newTimerTime = timerOpenButton.Timer.EndTimer.Subtract(currentTime);
        if (timerOpenButton.Timer.IsStart && timerOpenButton.Timer.EndTimer > currentTime)
        {
            timerTime = newTimerTime.TotalSeconds;
        }
        else
        {
            timerTime = 0;
            ChangeTimerText();
        }
    }

    private void Update()
    {
        if (currentTimerButton == null) return;
        if (isAddTime)
        {
            timerTime += currentTimeChangeSpeed;
            currentTimeChangeSpeed += deltaTimeChangeSpeed;
            ChangeTimerText();
        }

        if (isReduceTime && timerTime > 0)
        {
            timerTime -= currentTimeChangeSpeed;
            currentTimeChangeSpeed += deltaTimeChangeSpeed;
            ChangeTimerText();
        }

        if (timerTime < 0)
        {
            timerTime = 0;
        }

        if (currentTimerButton.Timer.IsStart && timerTime > 0)
        {
            timerTime -= Time.deltaTime;
            ChangeTimerText();
        }

        ChangeInteractableButtons();
    }

    private void ChangeInteractableButtons()
    {
        if (timerTime > 0 && !startTimeButton.interactable && !currentTimerButton.Timer.IsStart)
        {
            startTimeButton.interactable = true;
        }

        if (timerTime == 0 && startTimeButton.interactable && !currentTimerButton.Timer.IsStart)
        {
            startTimeButton.interactable = false;
        }
    }

    private void ChangeTimerText()
    {
        var time = TimeSpan.FromSeconds(timerTime);
        var timeStr = time .ToString(@"hh\:mm\:ss");
        timerText.SetText(timeStr);
    }

    public void StartTimer()
    {
        currentTimerButton.Timer.IsStart = true;
        currentTimerButton.Timer.EndTimer = DateTime.Now.AddSeconds(timerTime);
        addTimeButton.interactable = false;
        reduceTimeButton.interactable = false;
        startTimeButton.interactable = false;
        SaveController.SaveTimers(timersController.Timers);
        ReturnMainMenu();
    }

    public void RemoveTimer()
    {
        timersController.RemoveTimer(currentTimerButton);
        SaveController.SaveTimers(timersController.Timers);
        ReturnMainMenu();
    }

    public void ReturnMainMenu()
    {
        timersController.CloseTimer();
    }

    public void AddEndTimeStart()
    {
        isAddTime = true;
    }

    public void AddEndTimerEnd()
    {
        isAddTime = false;
        currentTimeChangeSpeed = startTimeChangeSpeed;
    }

    public void ReduceEndTimeStart()
    {
        isReduceTime = true;
    }

    public void ReduceEndTimerEnd()
    {
        isReduceTime = false;
        currentTimeChangeSpeed = startTimeChangeSpeed;
    }

    private void OnDestroy()
    {
        timersController.OnCurrentTimerChange -= OnChangeCurrentTimer;
    }
}
