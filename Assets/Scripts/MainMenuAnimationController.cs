﻿using UnityEngine;

public class MainMenuAnimationController : MonoBehaviour
{
    [SerializeField] private TimerButtonsAnimator timerButtonsAnimator;
    [SerializeField] private TimerAnimator timerAnimator;
    [SerializeField] private TimersController timersController;

    private void Awake()
    {
        timerButtonsAnimator.OnOpenFinish += OnTimersAnimationOpen;
        timerButtonsAnimator.OnCloseFinish += OnTimersAnimationClose;
        timerAnimator.OnCloseFinish += OnTimerAnimationClose;
        timerAnimator.OnOpenFinish += OnTimerAnimationOpen;
        timersController.OnTimerAdd += OnAddTimer;
        timersController.OnCurrentTimerChange += OnChangeCurrentTimer;
    }

    private void OnChangeCurrentTimer(TimerOpenButton timerOpenButton)
    {
        if (timerOpenButton != null)
        {
            timerButtonsAnimator.CloseAnimation();   
        }
        else
        {
            timerAnimator.CloseAnimation();
        }
    }

    private void OnAddTimer()
    {
        timerButtonsAnimator.OpenAnimation(timersController.TimersTransforms);
    }

    private void OnTimersAnimationOpen()
    {
        
    }

    private void OnTimersAnimationClose()
    {
        timerAnimator.OpenAnimation();
    }

    private void OnTimerAnimationOpen()
    {
        
    }

    private void OnTimerAnimationClose()
    {
        timerButtonsAnimator.OpenAnimation(timersController.TimersTransforms);
    }

    private void OnDestroy()
    {
        timerButtonsAnimator.OnOpenFinish -= OnTimersAnimationOpen;
        timerButtonsAnimator.OnCloseFinish -= OnTimersAnimationClose;
        timerAnimator.OnCloseFinish -= OnTimerAnimationClose;
        timerAnimator.OnOpenFinish -= OnTimerAnimationOpen;
        timersController.OnTimerAdd -= OnAddTimer;
        timersController.OnCurrentTimerChange -= OnChangeCurrentTimer;
    }
}
