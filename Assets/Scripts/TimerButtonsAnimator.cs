﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TimerButtonsAnimator : MonoBehaviour
{
    [SerializeField] private CanvasGroup timersGroup;
    [SerializeField] private float buttonAnimationDelay;
    [SerializeField] private float buttonMoveSpeed;
    [SerializeField] private float alphaAnimationSpeed;
    [SerializeField] private Button addTimerButton;

    public event Action OnOpenFinish;
    public event Action OnCloseFinish;

    public void OpenAnimation(RectTransform[] buttonTransforms)
    {
        addTimerButton.interactable = false;
        var oldPositionX = 0f;
        if (buttonTransforms.Length > 0)
        {
            oldPositionX = buttonTransforms[0].localPosition.x;
        }
        foreach (var buttonTransform in buttonTransforms)
        {
            var startPosition = buttonTransform.localPosition;
            startPosition = new Vector3(-buttonTransform.rect.width * 1.5f, startPosition.y, startPosition.z);
            buttonTransform.localPosition = startPosition;
        }

        timersGroup.alpha = 1f;
        StartCoroutine(AllButtonsAnimation(buttonTransforms, oldPositionX));
    }

    public void CloseAnimation()
    {
        StartCoroutine(HideTimersAnimation());
    }

    private IEnumerator HideTimersAnimation()
    {
        for (var a = 1f; a > 0f; a-=Time.deltaTime * alphaAnimationSpeed)
        {
            timersGroup.alpha = a;
            yield return new WaitForSeconds(Time.deltaTime);
        }

        timersGroup.alpha = 0f;
        OnCloseFinish?.Invoke();
    }

    private IEnumerator AllButtonsAnimation(RectTransform[] buttonTransforms, float finishXPosition)
    {
        foreach (var buttonTransform in buttonTransforms)
        {
            StartCoroutine(ButtonAnimation(buttonTransform, finishXPosition));
            yield return new WaitForSeconds(buttonAnimationDelay);
        }
        
        OnOpenFinish?.Invoke();
        addTimerButton.interactable = true;
    }

    private IEnumerator ButtonAnimation(RectTransform buttonTransform, float finishXPosition)
    {
        var localPosition = buttonTransform.localPosition;
        var y = localPosition.y;
        var z = localPosition.z;
        for (var x = localPosition.x; x < finishXPosition; x+=buttonMoveSpeed)
        {
            buttonTransform.localPosition = new Vector3(x, y, z);
            yield return new WaitForSeconds(Time.deltaTime);
        }
        buttonTransform.localPosition = new Vector3(finishXPosition, y, z);
    }
}
