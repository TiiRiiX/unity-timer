﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TimersController : MonoBehaviour
{
    public List<TimerOpenButton> TimersButtons { private set; get; }
    public Timer[] Timers => TimersButtons.Select(button => button.Timer).ToArray();
    public TimerOpenButton CurrentTimerOpenButton { private set; get; }
    public event Action OnTimerAdd;
    public event Action<TimerOpenButton> OnCurrentTimerChange;
    public RectTransform[] TimersTransforms => TimersButtons.Select(timer => timer.ButtonTransform).ToArray();

    [SerializeField] private Transform timerButtonsContainer;
    [SerializeField] private GameObject timerButtonPrefab;

    private void Start()
    {
        TimersButtons = new List<TimerOpenButton>();
        var loadedTimers = SaveController.LoadTimers();
        foreach (var timer in loadedTimers)
        {
            AddTimer(timer.EndTimer, timer.IsStart);
        }
        OnTimerAdd?.Invoke();
    }

    private void OpenTimer(TimerOpenButton current)
    {
        CurrentTimerOpenButton = current;
        OnCurrentTimerChange?.Invoke(current);
    }

    public void CloseTimer()
    {
        CurrentTimerOpenButton = null;
        OnCurrentTimerChange?.Invoke(null);
    }

    public void RemoveTimer(TimerOpenButton timerButton)
    {
        TimersButtons.Remove(timerButton);
        Destroy(timerButton.gameObject);
    }

    public void CreateTimer()
    {
        AddTimer(DateTime.MinValue, false);
        OnTimerAdd?.Invoke();
    }

    private void AddTimer(DateTime endTimer, bool isStart)
    {
        var timerButtonObj = Instantiate(timerButtonPrefab, timerButtonsContainer);
        var timerButton = timerButtonObj.GetComponent<TimerOpenButton>();
        timerButton.Timer = new Timer(endTimer, isStart);
        timerButton.OpenTimerButton.onClick.AddListener(() =>
        {
            OpenTimer(timerButton);
        });
        TimersButtons.Add(timerButton);
    }
}
