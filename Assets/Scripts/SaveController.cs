﻿using System.Linq;
using UnityEngine;

public class SaveController
{
    
    private const string timersKey = "timers";

    public static Timer[] LoadTimers()
    {
        if (!PlayerPrefs.HasKey(timersKey)) return new Timer[0];
        var timerContainer = JsonUtility.FromJson<TimerContainer>(PlayerPrefs.GetString(timersKey));
        return timerContainer.TimersStr.Select(timerStr => Timer.Parse(timerStr)).ToArray();
    }

    public static void SaveTimers(Timer[] timers)
    {
        var container = new TimerContainer();
        container.TimersStr = timers.Select(timer => timer.ToString()).ToArray();
        PlayerPrefs.SetString(timersKey, JsonUtility.ToJson(container));
        PlayerPrefs.Save();
    }
}
