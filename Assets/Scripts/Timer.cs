﻿using System;
using System.Globalization;

public class Timer
{
    public DateTime EndTimer;
    public bool IsStart;

    public Timer(DateTime endTime, bool isStart)
    {
        EndTimer = endTime;
        IsStart = isStart;
    }

    public override string ToString()
    {
        var startStr = IsStart ? "1" : "0";
        var timeStr = EndTimer.ToString("MM/dd/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
        return $"{startStr}|{timeStr}";
    }

    public static Timer Parse(string s)
    {
        char[] separators = { '|' }; 
        var parseParts = s.Split(separators, 2);
        var start = parseParts[0] == "1";
        var time = DateTime.ParseExact(parseParts[1], "MM/dd/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
        return new Timer(time, start);
    }
}

public class TimerContainer
{
    public string[] TimersStr;
}