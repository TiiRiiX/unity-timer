﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TimerOpenButton : MonoBehaviour
{
    public Timer Timer;

    public RectTransform ButtonTransform => buttonTransform;
    public Button OpenTimerButton => openTimerButton;

    [SerializeField] private RectTransform buttonTransform;
    [SerializeField] private Button openTimerButton;
    [SerializeField] private Color finishColor;

    private bool isFinishProcessing = false;
    
    private void Update()
    {
        if (Timer.IsStart && Timer.EndTimer < DateTime.Now && !isFinishProcessing)
        {
            FinishProcessing();
        }
    }

    private void FinishProcessing()
    {
        Timer.EndTimer = DateTime.MinValue;
        openTimerButton.image.color = finishColor;
        isFinishProcessing = true;
    }
}
